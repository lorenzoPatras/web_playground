const express = require('express')
const router = express.Router()
const { getPeople, createPerson, updatePerson, deletePerson } = require('../controllers/people')

router.get('/', getPeople)
router.post('/insert', createPerson)
router.put('/:id', updatePerson) // http://localhost:5000/api/people/2
router.delete('/:id', deletePerson)

// alternative to last two lines
// router.route('/:id).put(updatePerson).delete(deletePerson)


module.exports = router
const express = require('express')
const app = express()

function logger(req, res, next) {
  const method = req.method
  const url = req.url
  const time = new Date().getFullYear()
  console.log(method, url, time)
  next()
}

function secondLogger(_, _, next) {
  console.log('second logger')
  next()
}

app.use(logger, secondLogger) // express middleware

function thirdLogger(_, _, next) {
  console.log('third logger')
  next()
}

app.get('/', thirdLogger, (req, res) => {
  res.send('Home page')
})

app.get('/about', (req, res) => {
  res.send('about page')
})

app.get('/api/dummy', (req, res) => {
  res.json({name: "name1", number: 33})
})

// test http://localhost:5000/api/v1/query?name=john&age=23
app.get('/api/v1/query', (req, res) => {
  console.log(req.query) 
  res.send('hello world')
})



app.all('*', (req, res) => {
  res.status(404).send('<h1>resource not found</h1>')
})


app.listen(5000, () => {
  console.log('server is listening to port 5000')
})


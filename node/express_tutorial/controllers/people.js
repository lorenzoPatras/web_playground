let people = require('../data')

const getPeople = (req, res) => {
  res.status(200).json({ success: true, data: people })
}

const createPerson = (req, res) => {
  if (req.body.name) {
    let newPeople = [...people]
    newPeople.push({ id: 10, name: req.body.name })
    res
      .status(200)
      .json({ success: true, data: newPeople })
  } else {
    res
      .status(444)
      .send('incorrect req')
  }
}

const updatePerson = (req, res) => {
  const { id } = req.params
  if (!people.find((person) => { return person.id === Number(id) })) {
    return res
      .status(404)
      .json({ success: false, msg: 'not found' })
  }

  let newPeople = people.map((person) => {
    if (person.id === Number(id)) {
      person.name = req.body.name
    }
    return person
  })

  return res
    .status(200)
    .json(newPeople)
}

const deletePerson = (req, res) => {
  const { id } = req.params
  console.log(id);
  if (!people.find((person) => { return person.id === Number(id) })) {
    return res
      .status(404)
      .json({ success: false, msg: 'not found' })
  }

  let newPeople = people.filter((person) => {
    return person.id !== Number(id)
  })

  return res
    .status(200)
    .json(newPeople)
}

module.exports = { getPeople, createPerson, updatePerson, deletePerson }
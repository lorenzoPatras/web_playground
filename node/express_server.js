const express = require('express')

const app = express();

// app.use(express.static(__dirname + "/public"))

// app.use((req, res, next) => { // the middleware
//   console.log("in express middleware")
//   next();
// });

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.get('/', (req, res) => {
  console.log("req query: " + req.query); // the query of the request
  // req.body
  // req.header
  // req.params,
  res.send("get - root");
});

app.get('/profile', (req, res) => {
  res.send("get the profile");
});

app.post('/profile', (req, res) => {
  console.log(req.body);
  // send req.body to database
  res.send("Success");
});


app.listen(3000);
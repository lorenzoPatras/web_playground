const EventEmitter = require('events')

const customEmitter = new EventEmitter();

customEmitter.on('response', (name, id) => {
  console.log('other logic ' + name + ' ' + id)
})

customEmitter.on('response', (_, id) => {
  console.log('data received ' + (id + 3))
})

customEmitter.on('response', () => {
  console.log('other logic 2')
})


customEmitter.emit('response', 'dummy', 45)
// built in modules
const os = require('os') 
const path = require('path')
const {readFileSync, writeFileSync, readFile, writeFile} = require('fs')

// info about current user
const user = os.userInfo()
console.log(user)

// system uptime in seconds
console.log(os.uptime())

const currentOS = {
  name: os.type(),
  release: os.release(),
  memory: os.totalmem(),
  freeMem: os.freemem()
}

console.log(currentOS)

// prints the separator
console.log(path.sep)
const absolute = path.resolve(__filename)
console.log(absolute)

const first = readFileSync('./content/first.txt', 'utf8')
const second = readFileSync('./content/second.txt', 'utf8')

console.log(first)
console.log(second)

writeFileSync('./content/write_sync.txt', 'put text from js')

readFile('./content/first.txt','utf8' ,(err, first) => {
  if (err) {
    console.log(err)
    return
  }

  readFile('./content/second.txt', 'utf8', (err, second) => {
    if (err) {
      console.log(err)
      return
    }
    console.log('in the second file: ' + second)
  })

  console.log("content from first " + first)
})
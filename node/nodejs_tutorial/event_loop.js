const {readFile, writeFile} = require('fs')
const util = require('util')


const getText = (path) => {
  return new Promise((resolve, reject) => {
    readFile(path, 'utf8', (err, res) => {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

// getText('./content/first.txt')
//   .then((result) => console.log(result))
//   .catch((err) => console.log(err))
// console.log('start another task')

const start = async() => {
  try {
    const first = await getText('./content/first.txt')
    const second = await getText('./content/second.txt')
    console.log(first)
    console.log(second)

  } catch(err) {
    console.log(err)
  }
}
start()

const readFilePromise = util.promisify(readFile)
const writeFilePromise = util.promisify(writeFile)
const test = async() => {
  try {
    const first = await readFilePromise('./content/first.txt', 'utf8')
    const second = await readFilePromise('./content/second.txt', 'utf8')
    writeFilePromise('./content/result.txt', `result ${first} ${second}`)
    console.log('promisify ' + first)
    console.log('promisify ' + second)

  } catch(err) {
    console.log(err)
  }
}
test()
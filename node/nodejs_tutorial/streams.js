// create a big file to read it with streams
// const {writeFileSync} = require('fs')

// for (let i = 0; i < 100000; i++) {
//   writeFileSync('./content/big.txt', `hello world ${i}\n`, {flag: 'a'})
// }

const {createReadStream} = require('fs')

const stream = createReadStream('./content/big.txt', {highWaterMark: 90000})
stream.on('data', (result) => {
  console.log(result)
})

stream.on('error', (err) => {
  console.log(err)
})
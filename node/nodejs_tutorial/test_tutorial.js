const amount = 5

if (amount < 10) {
  console.log('small number')
} else {
  console.log('large number')
}

console.log('current dir: ' + __dirname)
console.log('current file: ' + __filename)

// setInterval(() => {
//   console.log('will show every 1sec')
// }, 1000)

console.log('process pid: ' + process.pid)
// answer to https://adventofcode.com/2015/day/1

const fs = require('fs');

const plusMinus = {
  '(': 1,
  ')': -1
}

const santaChallenge = (fileName) => {
  console.time('santaChallenge');
  fs.readFile(fileName, (err, data) => {
    if (err) {
      console.log(err);
    }

    const chars = data.toString().split('');
    let enteredBasementBefore = false;
    let iter = 0;
    const sum = chars.reduce((acc, chr) => {
      if (acc < 0 && !enteredBasementBefore) {
        console.log('First time when santa entered basement was at position ' + iter);
        enteredBasementBefore = true;
      }
      iter++;
      return acc + plusMinus[chr];
    }, 0);
    console.log(sum);
  });
  console.timeEnd('santaChallenge');
}

santaChallenge('santa.txt')

const fs = require('fs');

fs.readFile('./bla.txt', (err, data) => {
  if (err) {
    console.log(err);
  }

  console.log('async', data.toString());
});

const file = fs.readFileSync('./bla.txt');
console.log('sync', file.toString());

// append
// fs.appendFile('./bla.txt', " don't know what to say here", (err) => {
//   if (err) {
//     console.log(err);
//   }
// });

// write - overrides everything
// fs.writeFile('bla.txt', 'sad to see you go', (err) => {
//   if (err) {
//     console.log(err);
//   }
// })

// delete
// fs.unlink('bla.txt', (err) => {
//   if(err) {
//     console.log(err);
//   }
// })
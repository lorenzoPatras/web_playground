const express = require('express');
const bcrypt = require('bcrypt-nodejs');
const cors = require('cors');
const knex = require('knex');

// connect to db
const db = knex({
  client: 'pg',
  connection: {
    host: 'localhost',
    user: 'postgres',
    password: 'postgres',
    database: 'smart_brain'
  }
});

const app = express();
app.use(express.json());
app.use(cors());

app.listen(3001, () => {
  console.log('app is running on port 3001');
});

app.get('/', (req, res) => {
  res.json('hello');
});

app.post('/signin', (req, res) => {
  // async version of compare
  // bcrypt.compare(password, hashed, function (err, match) {
  //   console.log('compare result ', match);
  // });
  db.select('email', 'hash').from('login')
    .where({ email: req.body.email })
    .then(data => {
      const isValid = bcrypt.compareSync(req.body.password, data[0].hash);
      if (isValid) {
        return db.select('*').from('users')
          .where({ email: req.body.email })
          .then(user => {
            res.json(user[0]);
          })
          .catch(err => res.status(400).json('error at sign in'));
      } else {
        res.status(400).json('wrong credentials')
      }
    })
    .catch(err => res.status(400).json('wrong credentials'));
});

app.post('/register', (req, res) => {
  const { email, name, password } = req.body;
  const hash = bcrypt.hashSync(password);
  // async version
  // bcrypt.hash(password, null, null, function (err, hash) {
  //   console.log('hash result ', hash);
  // });
  db.transaction(trx => {
    trx.insert({
      hash: hash,
      email: email
    })
      .into('login')
      .returning('email')
      .then(loginEmail => {
        return trx('users')
          .returning('*')
          .insert({
            email: loginEmail[0],
            name: name,
            joined: new Date()
          })
          .then(user => {
            res.json(user[0]);
          })
      })
      .then(trx.commit)
      .catch(trx.rollback)
  })
    .catch(err => res.status(400).json(err)); // do not return err to client in real world :))
});

app.get('/profile/:userId', (req, res) => {
  const { userId } = req.params;
  db.select('*')
    .from('users')
    .where({
      id: userId
    })
    .then(user => {
      if (user.length) {
        console.log(user[0]);
      } else {
        res.status(400).json('user not found')
      }
    })
    .catch(err => res.status(500).json('general error'));
});

app.put('/image', (req, res) => {
  const { userId } = req.body;
  db('users')
    .where({ id: userId }) //('id', '=', userId)
    .increment('entries', 1)
    .returning('entries')
    .then(entries => {
      if (entries.length) {
        res.json(entries[0]);
      } else {
        res.status(400).json('user not found')
      }
    })
    .catch(err => res.status(500).json('general error'));
});

// for the database use
// CREATE TABLE users ( id serial PRIMARY KEY, name VARCHAR(100), email TEXT UNIQUE NOT NULL, entries BIGINT DEFAULT 0, joined TIMESTAMP NOT NULL );
// CREATE TABLE login ( id serial PRIMARY KEY, hash VARCHAR(100) NOT NULL, email TEXT UNIQUE NOT NULL );
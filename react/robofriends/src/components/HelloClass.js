import React, { Component } from 'react';

class HelloClass extends Component {
    render() {
        return (
            <div className='f1 tc'>
                <h1> Hello React World!</h1>
                <p>{this.props.greeting}</p>
            </div>
        );
    }
}


export default HelloClass;
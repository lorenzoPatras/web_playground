import React from 'react'
import Card from './Card.js'

const CardList = ({ robots }) => {
    // enable this to see ErrorBoundry functionality
    // if (true) {
    //     throw new Error('nonono');
    // }

    const cardsArray = robots.map((robot, i) => {
        return (<Card
            key={i}
            id={robot.id}
            name={robot.name}
            email={robot.email}
        />)
    });

    return (
        <div>
            {cardsArray};
        </div>
    );
}

export default CardList;
import React from 'react';

const HelloFunc = (props) => {
    return (
        <div className='f1 tc'>
            <h1> Hello React World!</h1>
            <p>{props.greeting}</p>
        </div>
    );
}


export default HelloFunc;
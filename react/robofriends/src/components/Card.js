import React from 'react'

const Card = ({ name, email, id }) => {
    return (
        // this is jsx not html and we need to import React for this
        // always make sure that this function returns only one element (in this case div)
        <div className='bg-light-green dib br3 bw2 pa3 ma2 grow shadow-5'>
            <img src={`https://robohash.org/${id}?200x200`} alt='robot pic' />
            <div>
                <h2>{name}</h2>
                <p>{email}</p>
            </div>

        </div>
    );
}

export default Card;
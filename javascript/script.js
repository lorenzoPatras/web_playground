let button = document.getElementById("enter");
let input = document.getElementById("userInput");
let ul = document.querySelector("ul");

const DELETE_CLASS = "delete";

function addElementToList() {
  if (input.value) {
    ul.appendChild(createListElement());
    input.value = "";
  }
}

function createBeleteButton() {
  let deleteBtn = document.createElement("button");
  deleteBtn.classList.add(DELETE_CLASS);
  deleteBtn.appendChild(document.createTextNode(DELETE_CLASS));
  deleteBtn.addEventListener("click", deleteClicked);
  return deleteBtn;
}

function createListElement() {
  let li = document.createElement("li");
  let deleteBtn = createBeleteButton();

  li.appendChild(document.createTextNode(input.value));
  li.appendChild(deleteBtn);
  li.addEventListener("click", liClicked);
  return li;
}

function buttonClicked() {
  addElementToList();
}

function enterPressed(event) {
  if (event.which === 13) {
    addElementToList();
  }
}

function liClicked(event) {
  event.target.classList.toggle("done");
}

function deleteClicked(event) {
  event.target.parentNode.parentNode.removeChild(event.target.parentNode);
}

button.addEventListener("click", buttonClicked);
input.addEventListener("keypress", enterPressed);

let allLis = document.querySelectorAll("li");
allLis.forEach((item, i) => {
  item.addEventListener("click", liClicked);
});

let allDeleteBtns = document.querySelectorAll(".delete");
allDeleteBtns.forEach((item, i) => {
  item.addEventListener("click", deleteClicked);
});

///// gradient code
let css = document.querySelector("h3");
let color1 = document.querySelector(".color1");
let color2 = document.querySelector(".color2");
let body = document.getElementById("gradient");

function changeGradient() {
  body.style.background =
	"linear-gradient(to right, "
	+ color1.value
	+ ", "
	+ color2.value
	+ ")";
  css.textContent = body.style.background + ";";
}

color1.addEventListener("input", changeGradient);

color2.addEventListener("input", changeGradient);

/// nodejs
var _ = require("lodash");
console.log(_);
var array = [1,2,3,4,5,6,7,8];
console.log('answer: ' + _.without(array, 3));

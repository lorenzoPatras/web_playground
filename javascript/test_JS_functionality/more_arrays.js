const array = [1, 2, 10, 15];

const newArray = [];
array.forEach((num, i) => {
  newArray.push(num * 2);
});
console.log("newArray " + newArray);

// map
const mapArray = array.map((num) => {
  return num * 2;
});
console.log("mapArray: " + mapArray);

const multBy3 = (a) => a * 3;
const map3Array = array.map(multBy3);
console.log("map3Array " + map3Array);

// filter
const filterArray = array.filter((num) => {
  return num > 5
});
console.log("filterArray " + filterArray);

const lessThan10 = (a) => a < 10;
const filterArray1 = array.filter(lessThan10);
console.log("filterArray1 " + filterArray1);

// reduce
const reduceArray = array.reduce((accumulator, num) => {
  return accumulator + num;
}, 0);
console.log("reduceArray " + reduceArray);


// play
// Complete the below questions using this array:
const initialArray = [
  {
    username: "john",
    team: "red",
    score: 5,
    items: ["ball", "book", "pen"]
  },
  {
    username: "becky",
    team: "blue",
    score: 10,
    items: ["tape", "backpack", "pen"]
  },
  {
    username: "susy",
    team: "red",
    score: 55,
    items: ["ball", "eraser", "pen"]
  },
  {
    username: "tyson",
    team: "green",
    score: 1,
    items: ["book", "pen"]
  },

];

//Create an array using forEach that has all the usernames with a "!" to each of the usernames
const usersArray = []
initialArray.forEach((user) => {
  usersArray.push({
    username: user.username + "!",
    team: user.team,
    score: user.score,
    items: user.items});
});

//Create an array using map that has all the usernames with a "? to each of the usernames
const qUsers = initialArray.map((user) => {
  return {
    username: user.username + "?",
    team: user.team,
    score: user.score,
    items: user.items};
});

//Filter the array to only include users who are on team: red
const redUsers = initialArray.filter((user) => {
  return user.team === "red";
});

//Find out the total score of all users using reduce
const totalScore = initialArray.reduce((acc, user) => {
  return acc + user.score;
}, 0);

//BONUS: create a new list with all user information, but add "!" to the end of each items they own.
const itemsUsersArray = initialArray.map((user) => {
  return {
    username: user.username,
    team: user.team,
    score: user.score,
    items: user.items.map((item) => {
      return item + "!";
    })
  };
});

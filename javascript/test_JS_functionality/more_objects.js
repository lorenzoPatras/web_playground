// reference type
[1] === [1]; // false
let obj1 = {value: 10};
let obj2 = obj1;
let obj3 = {value: 10};
obj1 === obj2; // true
obj1 === obj3; // false
obj1.value = 15;
console.log(obj2.value); // 15

// context vs scope
function b() {
  let a = 4; // scope
};

const obj4 = {
  a: function() {
    console.log(this); // refers to the context of object 4
  }
}

// this refers tot the context of the object. this keyword refers to what object you are inside at the time you call it

// instantiation
class Player {
  constructor(name, type) {
    this.name = name;
    this.type = type;
  }

  introduce() {
    console.log(`Hi I am ${this.name}`);
  }
}

class Wizzard extends Player {
  constructor(name, type) {
    super(name, type); // must be called whenever you extend a class
  }

  play() {
    console.log(`Wee I am a ${this.type}`);
  }
}

const wiz1 = new Wizzard("Tim", "Healer");

// object manipulation
let obj = {
  u0: "Santa",
  u1: "Rudolf",
  u2: "Mr. Grinch"
}

Object.keys(obj).forEach((key, index) => {
  console.log(key, obj[key]);
});

Object.values(obj).forEach((value) => {
  console.log(value);
});

Object.entries(obj).forEach((entry) => {
  console.log(entry[0] + " - " +  entry[1]);
});

// more looping
const basket = ["apples", "oranges", "grapes"]
const detailedBasket = {
  apples: 5,
  oranges: 10,
  grapes: 100
};

// for of - iterates over elements of an array
for (item of basket) {
  console.log(item);
}

// for in - enumerates properties of object
for (item in detailedBasket) {
  console.log(item + " - " + detailedBasket[item])
}

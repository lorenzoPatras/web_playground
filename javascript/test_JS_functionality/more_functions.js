// closure
const first = () => {
  const greet = "Hi";
  const second = () => {
    console.log(greet);
  }
  return second;
}

const newFunc = first();
newFunc()

// function currying
const multiply = (a, b) => a * b;
const curriedMultiply = (a) => (b) => a * b;
curriedMultiply(3); // equivalent to (b) => 3 * b;
curriedMultiply(3)(2); // returns 6
const multiplyBy2 = curriedMultiply(2);

// function composition
const compose = (f, g) => (a) => f(g(a));
const add3 = (num) => num + 3;
const mult4 = (num) => num * 4;
compose(add3, mult4)(5); // add3(mult4(5)); 23
compose(mult4, add3)(5); // mult4(add3(5)); 32


// play
//Closure: What does the last line return?
const addTo = x => y => x + y;
var addToTen = addTo(10);
console.log(addToTen(3)); // 13

//Currying: What does the last line return?
const curriedSum = (a) => (b) => a + b;
console.log(curriedSum(30)(1)) // 31

//Currying: What does the last line return?
const curriedSum1 = (a) => (b) => a + b;
const add5 = curriedSum1(5);
console.log(add5(12)); //17

//Composing: What does the last line return?
const compose1 = (f, g) => (a) => f(g(a));
const add1 = (num) => num + 1;
const add6 = (num) => num + 6;
console.log(compose1(add1, add6)(10)); // 17

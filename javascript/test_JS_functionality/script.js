// #1
function q1() {
    var a = 5;
    if(a > 1) {
        a = 3;
    }
    console.log(a); // 3
}

//#2
var a = 0;
function q2() {
    a = 5; // change outer a to 5
}

function q22() {
    console.log(a); // print whatever is the value of outer a
}


//#3
function q3() {
    window.a = "hello"; // change outer a to hello
}


function q32() {
    console.log(a);
}

//#4
var a = 1; // redeclare global var a
function q4() {
    var a = "test";
    console.log(a); // print test
}

//#5
var a = 2;
if (true) {
    var a = 5; // for some reason it does not redefine a
    console.log(a); // print 5
}
console.log(a); // print 2

// dynamic properties
const name = "name"
const obj = {
  [name]: "john",
  age: 22
}; // obj = {name: "john", age: 22}

const prop1 = "simon"
const prop2 = true;
const obj2 = {
  prop1: prop1,
  prop2: prop2
}

const obj3 = {prop1, prop2};

let person = {
    firstName : "John",
    lastName  : "Doe",
    age       : 50,
    eyeColor  : "blue"
};
let {firstName, lastName, age, eyeColor} = person;
